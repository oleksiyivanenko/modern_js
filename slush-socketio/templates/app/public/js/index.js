(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
exports.FADE_TIME = 150; // ms
exports.TYPING_TIMER_LENGTH = 400; // ms
exports.COLORS = [
  '#e21400', '#91580f', '#f8a700', '#f78b00',
  '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
  '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];

},{}],2:[function(require,module,exports){
var sockets = require('../sockets');

// Initialize varibles
var $window = $(window);
var $inputMessage = $('.inputMessage'); // Input message input box

var $loginPage = $('.login.page'); // The login page
var $usernameInput = $('.usernameInput'); // Input for username
var $currentInput = $usernameInput.focus();

$inputMessage.on('input', function() {
  sockets.chat.updateTyping();
});

// Click events

$window.keydown(function (event) {
  // Auto-focus the current input when a key is typed
  if (!(event.ctrlKey || event.metaKey || event.altKey)) {
    $currentInput.focus();
  }
  // When the client hits ENTER on their keyboard
  if (event.which === 13) {
    sockets.chat.tryToSendMessage();
  }
});

// Focus input when clicking anywhere on login page
$loginPage.click(function () {
  $currentInput.focus();
});

// Focus input when clicking on the message input's border
$inputMessage.click(function () {
  $inputMessage.focus();
});

},{"../sockets":8}],3:[function(require,module,exports){
module.exports = {
  chat: require('./chat')
};

},{"./chat":2}],4:[function(require,module,exports){
$(function() {

  require('./controllers');

});

},{"./controllers":3}],5:[function(require,module,exports){
var cfg = require('../config');
var $messages = $('.messages'); // Messages area

exports.addParticipantsMessage = function(data) {
  var message = '';
  if (data.numUsers === 1) {
    message += "there's 1 participant";
  } else {
    message += "there are " + data.numUsers + " participants";
  }
  this.log(message);
};

// Gets the color of a username through our hash function
function getUsernameColor (username) {
  // Compute hash code
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
    hash = username.charCodeAt(i) + (hash * Math.pow(2, 5)) - hash;
  }
  // Calculate color
  var index = Math.abs(hash % cfg.COLORS.length);
  return cfg.COLORS[index];
}

// Log a message
exports.log = function(message, options) {
  var $el = $('<li>').addClass('log').text(message);
  this.addMessageElement($el, options);
};

// Adds the visual chat message to the message list
exports.addChatMessage = function(data, options) {
  // Don't fade the message in if there is an 'X was typing'
  var $typingMessages = this.getTypingMessages(data);
  options = options || {};
  if ($typingMessages.length !== 0) {
    options.fade = false;
    $typingMessages.remove();
  }

  var $usernameDiv = $('<span class="username"/>')
    .text(data.username)
    .css('color', getUsernameColor(data.username));
  var $messageBodyDiv = $('<span class="messageBody">')
    .text(data.message);

  var typingClass = data.typing ? 'typing' : '';
  var $messageDiv = $('<li class="message"/>')
    .data('username', data.username)
    .addClass(typingClass)
    .append($usernameDiv, $messageBodyDiv);

  this.addMessageElement($messageDiv, options);
};

// Adds the visual chat typing message
exports.addChatTyping = function(data) {
  data.typing = true;
  data.message = 'is typing';
  this.addChatMessage(data);
};

// Removes the visual chat typing message
exports.removeChatTyping = function(data) {
  this.getTypingMessages(data).fadeOut(function () {
    $(this).remove();
  });
};

// Adds a message element to the messages and scrolls to the bottom
// el - The element to add as a message
// options.fade - If the element should fade-in (default = true)
// options.prepend - If the element should prepend
//   all other messages (default = false)
exports.addMessageElement = function(el, options) {
  var $el = $(el);

  // Setup default options
  if (!options) {
    options = {};
  }
  if (typeof options.fade === 'undefined') {
    options.fade = true;
  }
  if (typeof options.prepend === 'undefined') {
    options.prepend = false;
  }

  // Apply options
  if (options.fade) {
    $el.hide().fadeIn(cfg.FADE_TIME);
  }
  if (options.prepend) {
    $messages.prepend($el);
  } else {
    $messages.append($el);
  }
  $messages[0].scrollTop = $messages[0].scrollHeight;
};

// Prevents input from having injected markup
exports.cleanInput = function(input) {
  return $('<div/>').text(input).text();
};

// Gets the 'X is typing' messages of a user
exports.getTypingMessages = function(data) {
  return $('.typing.message').filter(function() {
    return $(this).data('username') === data.username;
  });
};

// Gets the color of a username through our hash function
exports.getUsernameColor = function(username) {
  // Compute hash code
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
    hash = username.charCodeAt(i) + (hash * Math.pow(2, 5)) - hash;
  }
  // Calculate color
  var index = Math.abs(hash % cfg.COLORS.length);
  return cfg.COLORS[index];
};


},{"../config":1}],6:[function(require,module,exports){
arguments[4][3][0].apply(exports,arguments)
},{"./chat":5}],7:[function(require,module,exports){
var cfg = require('../config');
var modules = require('../modules');

var connected = false;
var chat = io('localhost:3000/chat');
var username;

var $loginPage = $('.login.page'); // The login page
var $usernameInput = $('.usernameInput'); // Input for username
var $inputMessage = $('.inputMessage'); // Input message input box
var $currentInput = $usernameInput.focus();
var $chatPage = $('.chat.page'); // The chatroom page
// Prompt for setting a username
var typing = false;
var lastTypingTime;

// Sets the client's username
exports.setUsername = function() {
  username = modules.chat.cleanInput($usernameInput.val().trim());

  // If the username is valid
  if (username) {
    $loginPage.fadeOut();
    $chatPage.show();
    $loginPage.off('click');
    $currentInput = $inputMessage.focus();

    // Tell the server your username
    chat.emit('add user', username);
  }
};

// Sends a chat message
exports.sendMessage = function() {
  var message = $inputMessage.val();
  // Prevent markup from being injected into the message
  message = modules.chat.cleanInput(message);
  // if there is a non-empty message and a chat connection
  if (message && connected) {
    $inputMessage.val('');
    modules.chat.addChatMessage({
      username: username,
      message: message
    });
    // tell server to execute 'new message' and send along one parameter
    chat.emit('new message', message);
  }
};

// Updates the typing event
exports.updateTyping = function() {
  if (connected) {
    if (!typing) {
      typing = true;
      chat.emit('typing');
    }
    lastTypingTime = (new Date()).getTime();

    setTimeout(function () {
      var typingTimer = (new Date()).getTime();
      var timeDiff = typingTimer - lastTypingTime;
      if (timeDiff >= cfg.TYPING_TIMER_LENGTH && typing) {
        chat.emit('stop typing');
        typing = false;
      }
    }, cfg.TYPING_TIMER_LENGTH);
  }
};

exports.tryToSendMessage = function() {
  if (username) {
    this.sendMessage();
    chat.emit('stop typing');
    typing = false;
  } else {
    this.setUsername();
  }
};

// chat events

// Whenever the server emits 'login', log the login message
chat.on('login', function (data) {
  connected = true;
  // Display the welcome message
  var message = "Welcome to chat.sio Chat – ";
  modules.chat.log(message, {
    prepend: true
  });
  modules.chat.addParticipantsMessage(data);
});

// Whenever the server emits 'new message', update the chat body
chat.on('new message', function (data) {
  modules.chat.addChatMessage(data);
});

// Whenever the server emits 'user joined', log it in the chat body
chat.on('user joined', function (data) {
  modules.chat.log(data.username + ' joined');
  modules.chat.addParticipantsMessage(data);
});

// Whenever the server emits 'user left', log it in the chat body
chat.on('user left', function (data) {
  modules.chat.log(data.username + ' left');
  modules.chat.addParticipantsMessage(data);
  modules.chat.removeChatTyping(data);
});

// Whenever the server emits 'typing', show the typing message
chat.on('typing', function (data) {
  modules.chat.addChatTyping(data);
});

// Whenever the server emits 'stop typing', kill the typing message
chat.on('stop typing', function (data) {
  modules.chat.removeChatTyping(data);
});

},{"../config":1,"../modules":6}],8:[function(require,module,exports){
arguments[4][3][0].apply(exports,arguments)
},{"./chat":7}]},{},[4])