var sockets = require('../sockets');

// Initialize varibles
var $window = $(window);
var $inputMessage = $('.inputMessage'); // Input message input box

var $loginPage = $('.login.page'); // The login page
var $usernameInput = $('.usernameInput'); // Input for username
var $currentInput = $usernameInput.focus();

$inputMessage.on('input', function() {
  sockets.chat.updateTyping();
});

// Click events

$window.keydown(function (event) {
  // Auto-focus the current input when a key is typed
  if (!(event.ctrlKey || event.metaKey || event.altKey)) {
    $currentInput.focus();
  }
  // When the client hits ENTER on their keyboard
  if (event.which === 13) {
    sockets.chat.tryToSendMessage();
  }
});

// Focus input when clicking anywhere on login page
$loginPage.click(function () {
  $currentInput.focus();
});

// Focus input when clicking on the message input's border
$inputMessage.click(function () {
  $inputMessage.focus();
});
