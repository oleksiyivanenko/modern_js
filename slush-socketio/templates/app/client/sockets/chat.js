var cfg = require('../config');
var modules = require('../modules');

var connected = false;
var chat = io('localhost:3000/chat');
var username;

var $loginPage = $('.login.page'); // The login page
var $usernameInput = $('.usernameInput'); // Input for username
var $inputMessage = $('.inputMessage'); // Input message input box
var $currentInput = $usernameInput.focus();
var $chatPage = $('.chat.page'); // The chatroom page
// Prompt for setting a username
var typing = false;
var lastTypingTime;


// Sets the client's username
exports.setUsername = function() {
  username = modules.chat.cleanInput($usernameInput.val().trim());

  // If the username is valid
  if (username) {
    $loginPage.fadeOut();
    $chatPage.show();
    $loginPage.off('click');
    $currentInput = $inputMessage.focus();

    // Tell the server your username
    chat.emit('add user', username);
  }
};

// Sends a chat message
exports.sendMessage = function() {
  var message = $inputMessage.val();
  // Prevent markup from being injected into the message
  message = modules.chat.cleanInput(message);
  // if there is a non-empty message and a chat connection
  if (message && connected) {
    $inputMessage.val('');
    modules.chat.addChatMessage({
      username: username,
      message: message
    });
    // tell server to execute 'new message' and send along one parameter
    chat.emit('new message', message);
  }
};

// Updates the typing event
exports.updateTyping = function() {
  if (connected) {
    if (!typing) {
      typing = true;
      chat.emit('typing');
    }
    lastTypingTime = (new Date()).getTime();

    setTimeout(function () {
      var typingTimer = (new Date()).getTime();
      var timeDiff = typingTimer - lastTypingTime;
      if (timeDiff >= cfg.TYPING_TIMER_LENGTH && typing) {
        chat.emit('stop typing');
        typing = false;
      }
    }, cfg.TYPING_TIMER_LENGTH);
  }
};

exports.tryToSendMessage = function() {
  if (username) {
    this.sendMessage();
    chat.emit('stop typing');
    typing = false;
  } else {
    this.setUsername();
  }
};

// chat events

// Whenever the server emits 'login', log the login message
chat.on('login', function (data) {
  connected = true;
  // Display the welcome message
  var message = "Welcome to chat.sio Chat – ";
  modules.chat.log(message, {
    prepend: true
  });
  modules.chat.addParticipantsMessage(data);
});

// Whenever the server emits 'new message', update the chat body
chat.on('new message', function (data) {
  modules.chat.addChatMessage(data);
});

// Whenever the server emits 'user joined', log it in the chat body
chat.on('user joined', function (data) {
  modules.chat.log(data.username + ' joined');
  modules.chat.addParticipantsMessage(data);
});

// Whenever the server emits 'user left', log it in the chat body
chat.on('user left', function (data) {
  modules.chat.log(data.username + ' left');
  modules.chat.addParticipantsMessage(data);
  modules.chat.removeChatTyping(data);
});

// Whenever the server emits 'typing', show the typing message
chat.on('typing', function (data) {
  modules.chat.addChatTyping(data);
});

// Whenever the server emits 'stop typing', kill the typing message
chat.on('stop typing', function (data) {
  modules.chat.removeChatTyping(data);
});
