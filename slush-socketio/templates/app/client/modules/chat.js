var cfg = require('../config');
var $messages = $('.messages'); // Messages area

exports.addParticipantsMessage = function(data) {
  var message = '';
  if (data.numUsers === 1) {
    message += "there's 1 participant";
  } else {
    message += "there are " + data.numUsers + " participants";
  }
  this.log(message);
};

// Gets the color of a username through our hash function
function getUsernameColor (username) {
  // Compute hash code
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
    hash = username.charCodeAt(i) + (hash * Math.pow(2, 5)) - hash;
  }
  // Calculate color
  var index = Math.abs(hash % cfg.COLORS.length);
  return cfg.COLORS[index];
}

// Log a message
exports.log = function(message, options) {
  var $el = $('<li>').addClass('log').text(message);
  this.addMessageElement($el, options);
};

// Adds the visual chat message to the message list
exports.addChatMessage = function(data, options) {
  // Don't fade the message in if there is an 'X was typing'
  var $typingMessages = this.getTypingMessages(data);
  options = options || {};
  if ($typingMessages.length !== 0) {
    options.fade = false;
    $typingMessages.remove();
  }

  var $usernameDiv = $('<span class="username"/>')
    .text(data.username)
    .css('color', getUsernameColor(data.username));
  var $messageBodyDiv = $('<span class="messageBody">')
    .text(data.message);

  var typingClass = data.typing ? 'typing' : '';
  var $messageDiv = $('<li class="message"/>')
    .data('username', data.username)
    .addClass(typingClass)
    .append($usernameDiv, $messageBodyDiv);

  this.addMessageElement($messageDiv, options);
};

// Adds the visual chat typing message
exports.addChatTyping = function(data) {
  data.typing = true;
  data.message = 'is typing';
  this.addChatMessage(data);
};

// Removes the visual chat typing message
exports.removeChatTyping = function(data) {
  this.getTypingMessages(data).fadeOut(function () {
    $(this).remove();
  });
};

// Adds a message element to the messages and scrolls to the bottom
// el - The element to add as a message
// options.fade - If the element should fade-in (default = true)
// options.prepend - If the element should prepend
//   all other messages (default = false)
exports.addMessageElement = function(el, options) {
  var $el = $(el);

  // Setup default options
  if (!options) {
    options = {};
  }
  if (typeof options.fade === 'undefined') {
    options.fade = true;
  }
  if (typeof options.prepend === 'undefined') {
    options.prepend = false;
  }

  // Apply options
  if (options.fade) {
    $el.hide().fadeIn(cfg.FADE_TIME);
  }
  if (options.prepend) {
    $messages.prepend($el);
  } else {
    $messages.append($el);
  }
  $messages[0].scrollTop = $messages[0].scrollHeight;
};

// Prevents input from having injected markup
exports.cleanInput = function(input) {
  return $('<div/>').text(input).text();
};

// Gets the 'X is typing' messages of a user
exports.getTypingMessages = function(data) {
  return $('.typing.message').filter(function() {
    return $(this).data('username') === data.username;
  });
};

// Gets the color of a username through our hash function
exports.getUsernameColor = function(username) {
  // Compute hash code
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
    hash = username.charCodeAt(i) + (hash * Math.pow(2, 5)) - hash;
  }
  // Calculate color
  var index = Math.abs(hash % cfg.COLORS.length);
  return cfg.COLORS[index];
};

