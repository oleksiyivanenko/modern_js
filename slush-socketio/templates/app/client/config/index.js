exports.FADE_TIME = 150; // ms
exports.TYPING_TIMER_LENGTH = 400; // ms
exports.COLORS = [
  '#e21400', '#91580f', '#f8a700', '#f78b00',
  '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
  '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];
