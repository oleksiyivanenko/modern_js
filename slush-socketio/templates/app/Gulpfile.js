var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var watch = require('gulp-watch');
var browserify = require('gulp-browserify');
var jshint = require('gulp-jshint');
var livereload = require('gulp-livereload');


//register nodemon task
gulp.task('nodemon', function () {
  nodemon({
    script: './index.js',
    env: {
      'NODE_ENV': 'development'
    }
  })
    .on('restart');
});

// Rerun the task when a file changes
gulp.task('watch', function () {
  livereload.listen();

  gulp.src(['*.js', 'routes/*.js', 'views/**/*.*', 'public/**/*.*', 'client/**/*.js'], {read: true})
    .pipe(watch(['*.js', 'routes/*.js', 'views/**/*.*', 'public/**/*.*', 'client/**/*.js'], function (file) {
      livereload.changed(file.path);
    }));
});

//browserify client js files
gulp.task('browserify', function() {
  gulp.src('client/index.js')
    .pipe(browserify())
    .pipe(gulp.dest('public/js'));
});

//lint js files
gulp.task('lint', function () {
  gulp.src(['*.js', 'routes/*.js', 'client/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});


// The default task (called when you run `gulp` from cli)
gulp.task('default', ['lint', 'nodemon', 'watch']);
