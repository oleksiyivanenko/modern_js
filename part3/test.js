var should = require('should');
var utils = require('./utils.js');

describe('Utils', function() {
    describe('#clone()', function() {
        it('should return deep copy of the object', function() {
            var proto = {p: 0};

            var obj = Object.create(proto);
            obj.x = {z: 1};
            obj.y = [{z: 2}, {z: 3}];

            var clone = utils.clone(obj);

            clone.should.eql(obj, 'Structure equality');
            clone.should.not.equal(obj);
            clone.x.should.not.equal(obj.x);
            clone.y.should.not.equal(obj.y);
            clone.y[0].should.not.equal(obj.y[0]);
            clone.y[1].should.not.equal(obj.y[1]);
            Object.getPrototypeOf(clone).should.equals(Object.getPrototypeOf(obj));
            utils.clone.bind(null, 5).should.throw('The method requires object but receive number');
        });
    });
});

// TODO напишите остальные тесты
// документация по тестам mocha - http://mochajs.org
// документация по библиотеке should https://github.com/tj/should.js
