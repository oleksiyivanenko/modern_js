/**
 * Модуль с утилитами
 */

function TypeMismatchError(arg, type) {
    var actualType = typeof arg;
    if(arg === null) actualType = 'null';
    this.name = 'TypeMismatch';
    this.message = 'The method requires' + type + ' but receive ' + actualType;
}


/**
 * Метод clone возвращает глубокую копию объкта
 * (не забываем про прототип, его нужно так же наследовать)
 * @param obj {Object} - объект который следует клонировать
 * @returns {Object} - клон объекта
 * @throws {TypeMismatchError} - исключение если передан не объект
 */
exports.clone = function(obj) {
    var res;
    throw new TypeMismatchError(obj, 'object');
    return res;
};

/**
 * Метод extend расширяет один объект свойствами другого
 * @param src {Object} - объект свойств
 * @param dst {Object} - расширяемый объект
 * @returns {Object} - объект расширенный свойствами другого объекта
 * @throws {TypeMismatchError} - исключение если переданы не объекты
 */
exports.extend = function(src, dst) {};

/**
 * Метод max возвращает максимальное значение массива
 * (можно использовать метод Math.max, который возвращает максимальный элемент последовательности)
 * @param arr {Array} - входящий числовой массив
 * @returns {Number} - максимальный элемент массива
 * @throws {TypeMismatchError} - исключение если передан не числовой массив
 */
exports.max = function(arr) {};

/**
 * Метод removeDuplicates удаляет одинаковые элементы из массива
 * @param arr {Array} - исходный массив
 * @returns {Array} - массив без повторений
 * @throws {TypeMismatchError} - исключение если передан не массив
 */
exports.removeDuplicates = function(arr) {};

/**
 * Метод toArray превратит массивоподобный объект (например arguments) в массив
 * @param obj {Object} - входящий массивоподобный объект
 * @returns {Array} - преобразованный массив
 * @throws {TypeMismatchError} - исключение если передан не массивоподобный объект
 */
exports.toArray = function(obj) {};

/**
 * Метод clear удалит из массива "пустые" объекты - null, undefined, '', [], {}
 * @param arr {Array} - входящий массив
 * @returns {Array} - преобразованный массив
 * @throws {TypeMismatchError} - исключение если передан не массив
 */
exports.clear = function(arr) {};

/**
 * Метод isDataObject который проверяет является ли объект простым объектом с данными без методов и наследований
 * созданный с помощью декларативной записи {} или, что то же самое с помощью new Object(), Object.create([null])
 * @param obj {Object} - входящий объект
 * @returns {boolean} - является ли объект объектом данных
 */
exports.isDataObject = function(obj) {};

/**
 * Метод getMethods который возвращает все методы данного объекта включая/исключая прототипы
 * @param obj {*} - входящий объект
 * @param recursive {boolean} - включать ли методы цепочки прототипов
 * @returns {Array} - массив методов объекта
 */
exports.getMethods = function(obj, recursive) {};

/**
 * Метод genId генератор последовательности натуральных чисел
 * @returns {Number} - следующее натуральное число
 */
exports.genId = function() {};

/**
 * Метод getType определяет тип переменной.
 * @param val - тестируемое значение
 * @returns {String} - результат должен быть одной из строк:
 * 'undefined', 'boolean' (для true/false), 'null', 'number', 'string', 'function', 'array', 'array-like', 'object'
 * array-like — это массивоподобный объект, то есть объект, у которого есть неотрицательное свойство length и элементы с 0 до length - 1.
 */
exports.getType = function(val) {};

/**
 * Метод fold принимает арифметический метод первым аргументом и далее числовые аргументы
 * и производит свертку этих аргументов указаным методом
 * @param fun {Function} - арифметический метод свертки
 * @return {Number} - результат свертки
 */
exports.fold = function(fun) {};

/**
 * Метод foldFun работает как предыдущий, но каждый его аргумент должен задаваться в отдельных скобках
 * @param fun {Function} - арифметический метод свертки
 * @return {*} - результат свертки
 */
exports.foldFun = function(fun) {};

/**
 * Метод bind - свою версию функции bind
 * (можно использовать apply и call контекст должен привязываться как и в bind и не зависеть от метода вызова функции)
 * @param scope {Object} - новый вызывающий объект либо null
 * @returns {Function} - функцию-обертку с привязанным контекстом
 */
exports.bind = function(scope) {};

/**
 * Метод createClass который принимает свойства и методы объекта, а так же прототип и примеси (mixins)
 * должно быть реализован механизм вызова метода из прототипа в переопределенном методе
 * @param props {Object} - объект содержащий
 * @param proto {Object} - объект прототипа либо null
 * @param mixins {Object} - объект содержащий примеси
 * @returns {Function} - возвращает функцию конструктор, должен работать также как фабрика
 */
exports.createClass = function(props, proto, mixins) {};

/**
 * Класс Binary содержит в себе представление числа в двоичной форме
 * @class Binary
 * @param num {Number} - десятичное целое число
 */
exports.Binary = function(num) {};
