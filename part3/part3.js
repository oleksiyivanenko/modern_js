/**
 * Функции
 */

/**
 * Функции объявляются с помощью ключевого слова function
 * в скобках содержатся аргументы функции
 * Объявленная таким образом функция доступна во всей области видимости
 */
function foo(bar) {
    bar();
    return bar;
}

/**
 * функции являются объектами класса Function,
 * а значит могут быть присвоены переменным, свойствам объектов, элементам массива,
 * переданы как аргументы или возвращены как результат работы других функций.
 * Тут мы создаем анонимную функцию и присваиваем ее значение переменной.
 * Обявленная таким образом функция доступна с момента объявления.
 */
var baz = function() {
    console.log("baz");
};

var result = baz(); // выведет baz и вернет undefined так как нет ключевого слова return
result == null; // true
result = foo(baz); // выведет baz и вернет функцию bar
result(); // выведет baz и вернет undefined - так как совпадает с функцией baz

/**
 * Класс Function
 */
var fun = new Function('x', 'y', 'return x + y;');
var fun1 = new Function('x, y', 'return x + y;');
var fun2 = Function('x', 'y', 'return x + y;');

fun(1, 2); // 3
fun1(1, 2); // 3
fun2(1, 2); // 3

/**
 * Аргументы функции
 */
function someFun(a, b) {
    console.log(arguments.length);
    console.log(a, arguments[0]);
    console.log(b, arguments[1]);
    console.log(arguments[2]);
}

someFun(1, 2, 3);
// 3 - количество переданых аргументов функции
// 1 1 - первый аргумент по имени и из объекта аргументов
// 2 2 - второй аргумент по имени и из объекта аргументов
// 3 - третий аргумент из объекта аргументов
someFun.length; // 2 - количество объявленых аргументов функции

function sum() {
    var s = 0;
    for(var i = 0, l = arguments.length; i < l; i ++) {
        s += arguments[i];
    }
    return s;
} // считает сумму переданых аргументов
sum(1, 2); // 3
sum(1, 2, 3); // 6

function sum2(a, b) {
    a = a || 0;
    b = b || 1;
    var c = arguments[2] || 0;
    return a + b + c;
} // определяет аргументы по умолчанию

sum2(); // 1
sum2(1); // 2
sum2(1, 2, 3); // 6

/**
 * Ссылка на функцию
 */
var count = function() {
    count.called = count.called + 1 || 1;
    console.log(count.called, count.prop);
}
count.prop = 1;

count(); // 1 1
count(); // 2 1
count.called; // 2

[1, 2, 3, 4, 5].map(function fact(n) {
    if(n === 0) return 1;
    return n * fact(n - 1);
}); // [1, 2, 6, 24, 120]

/**
 * Область видимости
 * это объект куда в качестве свойств помещаются объявленные переменные и функци
 * функции и переменные объявленные в теле скрипта помещаются в глобальную область видимости - объект global или window
 * кроме того функции сами по себе имеют свою область видимости
 */
var val = 1; // определит переменную в глобальной области видимости
var vaz = 1;
function changeVal() {
    val = 2; // изменит переменную в глобальной области видимости
    var vaz = 2; // создаст переменную в локальной области видимости
    console.log(val, vaz);
}
changeVal(); // выведет 2 2
console.log(val, vaz); // выведет 2 1

/**
 * Замыкания
 */
function outer() {
    var outerVar = 1;
    function inner() {
        console.log(outerVar);
    }
    return inner;
}
var inner = outer(); // область видимости функции outer сохранится даже после завершения её выполнения
inner(); // 1 - область видимости outerVar будет доступна функции inner
// после выполнения функции иннер области видимости обеих функций будут уничтожены сборщиком мусора

/**
 * Ключевое слово this, контекст выполнения
 */
x = 1;
y = 0;

function testThis(y) {
    this.y = y;
    console.log(this.x);
}

var first = {
    x: 2,
    testThis: testThis // тут мы привязали функцию к объекту
};

var second = {x: 3};

testThis(2); // 1 - вызов функции (метода глобального объекта) this будет указывать на глобальный объект в strict mode выбросит typeError
y; // 2
first.testThis(2); // 2 - вызов метода this будет указывать на вызывающий объект
first.y; // 2

testThis.call(second, 2); // 3 - call привяжет функцию testThis к объекту second и передаст ей список аргументов
testThis.apply(second, [2]); // 3 - apply сделает то же самое, но список аргументов передаст в виде массива
var testBindThis = testThis.bind(second); // создаст новую функцию-обертку и привяжет this к объекту second
testBindThis(2); // 3 - в даном случае this будет указывать на second
var testBindThis2 = testThis.bind(second, 2); // так же привяжет аргумент к контексту
testBindThis2(); // 3 - теперь функция вызывается вообще без аргументов
second.y; // 2

first.testThat = testBindThis2;
first.testThat(); // 3 - функция обертка уже не содержит this в своем коде, вместо this внутренней функции подставляется объект second
testBindThis2.apply(first); // 3
testBindThis2.call(first); // 3
testBindThis2.bind(first)(); // 3 - функция обертка не содержит this потому повторная привязка ничего не дает

new testThis(2); // вызов конструктора вернет новый объект { y: 2 } с прототипом функции и выведет undefined

/**
 * Прототип объекта, свойство prototype функции
 */
testThis.prototype; // {} - по умолчанию содержит пустой объект
testThis.prototype.x = 4;

var resObj1 = new testThis(0);
resObj1; // { y: 0 } - // обект содержит только свойство y
resObj1.x; // 4 - свойство x берется из прототипа

var resObj2 = new testThis(1);
resObj2; // { y: 1 }
resObj2.x; // 4

resObj1.x = 3; // переопределяем свойство x для первого объекта
resObj1; // { y: 0, x: 3 } - теперь сам объект содержит свойство x
resObj2; // { y: 1 }
resObj2.x; // 4 - свойство x второго объекта все так же берется из прототипа

delete resObj1.x; // удалим свойство x из первого объекта
resObj1.x; // 4 - теперь это свойство снова будет браться из прототипа

testThis.prototype.x = 5; // перезапишем свойство x  в прототипе
resObj1.x; // 5
resObj2.x; // 5

testThis.prototype.xSquare = function() {
    return this.x * this.x;
};

resObj1.xSquare(); // 25 - берет x из прототипа
resObj2.x = 1; // перезапишем свойство x в объекте
resObj2.xSquare(); // 1 - this указывает на объект resObj2

testThis.info = function() {
    console.log('this class is made for tests');
}; // статический метод класса не требующий экземпляра объекта

/**
 * Объект с неизвестным конструктором
 */
var proto = Object.getPrototypeOf(resObj1); // вернет прототип объекта
proto; // { x: 5 }
proto.x = 7; // теперь с ним можно работать без ссылки на функцию
resObj1.x; // 7
resObj2.x; // 7

var constr = resObj1.constructor; // так можно получить ссылку на функцию-конструктор
var resObj3 = new constr(); // теперь можно создавать объекты того же типа
constr.prototype.x = 9; // и менять свойства прототипа
resObj1.x; // 9
resObj2.x; // 9
resObj3.x; // 9

/**
 * Перечисление свойств объекта
 */
Object.keys(resObj1); // [ 'y' ] - массив перечисляемых свойств объекта
Object.getOwnPropertyNames(resObj1); // [ 'y' ] - массив свойств объекта

for(var keyName in resObj1) console.log(keyName); // y x - пройдется так же по свойствам прототипа
for(var key in resObj1) if(resObj1.hasOwnProperty(key)) console.log(key); // y - исключит свойства прототипа

/**
 * Object.create - еще один способ создания объектов
 */
proto; // { x: 7 }
var resObj4 = Object.create(proto); // можно создать объект и назначить ему прототип без функции конструктора

resObj4.y = 0; // можно назначать свойства вручную
resObj4; // { y: 0 }
resObj4.x; // 7

/**
 * Преобразования объектов к примитивам
 * Object.prototype.valueOf и Object.prototype.toString
 */

prim1 = {
    x: 1
};

prim2 = {
    x: 2
}

prim2 - prim1; // NaN - объекты по умолчанию преобразуются к неопределенности

// Строковое преобразование, используется когда на месте объекта ожидается строка
prim1.toString = function() {
    return this.x; // строковое преобразование, должно возвращать примитив, но не обязательно строку
};

// Преобразование в примитив, используется когда ожидается примитив, если оно не определено то используется toString
prim2.valueOf = function() {
    return this.x;
};

prim2 - prim1; // 1 - теперь объекты будут приведены к примитивным значениям
prim2 + prim1; // 3

/**
 * Еще одна сумма
 */
function sum3(a) {
    var s = a;

    function add(b) {
        a += b;
        return add;
    }

    add.toString = function() {
        return s;
    }

    return add;
}

sum3(1)(2); // 3
sum3(1)(2)(3); // 6
sum3(1)(2)(3)(4); // 10

/**
 * Операторы && и ||
 */
sarah = {
    getMoney: function(money) {
        this.money = money;
        return money >= 75;
    },
    showBoobs: function showBoobs() {
        console.log('weeeeee');
        return this.money - 75;
    }
};

sarah && sarah.getMoney(100) && sarah.showBoobs(); // выведет weeeeee и вернет 25
sarah && sarah.getMoney(25) && sarah.showBoobs(); // вернет false, sarah.showBoobs не будет выполнено

var el;
el = el || 0; // 0 - элемент не определен, вернется второй операнд
el = el || 1; // 1 - элемент определен, но при конвертации типов 0 имеет ложное значение, вернется второй операнд
el = el || 2; // 1 - элемент определен и равен единице, которая при конвертации типов превратится в правду, вернется первый операнд

var el1;
el1 = el1 != null ? el1 : 1; // если нужно инициализировать чем-то отличным от 0, и 0 - возможное значение

/**
 * Приведение примитивных типов проводится в зависимости от ситуации в которой оно происходит.
 */

/**
 * Строковое - при конкатенации и когда ожидается строка
 */
1 + 'string'; // '1string'

/**
 * Числовое - при математических операциях и сравнениях (кроме строгих)
 */
1 - true; // 0
1 + false; // 1
1 - ' \t  2  \n '; // -1
1 - ''; // 1
1 - 'a'; // NaN

/**
 * Специальные значения null и undefined
 * преобразуются к 0 и NaN соответственно при операциях сравнения >, >=, <, <=
 * но ведут себя особым образом при операции сравнения ==
 */
undefined > 0; // false - преобразуется к NaN а потому не сравним
undefined == 0; // false

undefined == null; // true - особый случай прописаный в спецификации,
// неопределенности нестрого равны между собой но не равны ничему другому

null >= 0; // true - null преобразуется в 0
null <= 0; // false - тоже правда
null > 0; // false
null < 0; // false
null == 0; // false - null не равен ничему кроме себя и undefined
// потому null больше равне нуля, меньше равен нуля, но не больше и не равен нулю

/**
 * Логическое - в логигическом контексте if, while, for
 */
if(null) console.log('код не выполнится'); // null -> false
if(undefined) console.log('код не выполнится'); // undefined -> false
if(0) console.log('код не выполнится'); // 0 -> false
if(-1) console.log('выполнится'); // все числа кроме 0 преобразовываются в true
if('') console.log('код не выполнится'); // '' -> false
if('0') console.log('выполнится'); // непустые строки преобразуются в true
if({}) console.log('выполнится'); // любые объекты преобразовываются в true

/**
 * Проверка типов, typeof и instanceof
 */
typeof 1.2; // 'number'
typeof 'str'; // 'string'
typeof true; // 'boolean'
typeof undefined; // 'undefined'
typeof undefinedVariable; // 'undefined'
typeof null; // 'object' - это баг

function Person(name) {this.name = name};
function Pet(name) {this.name = name};

typeof Person; // 'function'
typeof {}; // 'object'
typeof []; // 'object'

typeof new Person(); // 'object'
typeof new Pet(); // 'object'

obj = {};
obj instanceof Object; // true
[] instanceof Object; // true
[] instanceof Array; // true
Array.isArray([]); // true

new Person() instanceof Person; // true
new Person() instanceof Object; // true
new Pet() instanceof Pet; // true

null instanceof Object; // false

/**
 * Обработка и создание ошибок
 */
function MyError(message) {
    this.name = 'generatedExample';
    this.message = message;
}
MyError.prototype = new Error();
MyError.prototype.constructor = MyError;

try {
    throw new MyError('Throw error as an example');
} catch(error) {
    if(error instanceof MyError) {
        console.log(error.name, ':', error.message, '\n', error.stack);
    } else {
        throw error;
    }
} finally {
    console.log('выполнится в любом случае');
}

/**
 * bower install es5-shim
 */
